<!DOCTYPE html>
<html>
<head>
	<title>My CRUD Application</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

	<?php
		include('include/connect_db.php');

		include('include/create_table.php');

		if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['insert'])) {
			include('include/insert_data.php');
		}

		if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update'])) {
			include('include/update_data.php');
		}

		if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['delete'])) {
			include('include/delete_data.php');
		}

		include('include/read_data.php');
	?>

	<h2>Create</h2>
	<form method="post">
		<label for="name">Name:</label>
		<input type="text" name="name" required>

		<label for="email">Email:</label>
		<input type="email" name="email" required>

		<label for="password">Password:</label>
		<input type="text" name="password" required>

		<input type="submit" name="insert" value="Insert">
	</form>

	<h2>Update</h2>
	<form method="post">
		<label for="id">ID:</label>
		<input type="number" name="id" required>

		<label for="name">Name:</label>
		<input type="text" name="name">

		<label for="email">Email:</label>
		<input type="email" name="email">

		<label for="password">Password:</label>
		<input type="text" name="password" required>

		<input type="submit" name="update" value="Update">
	</form>

	<h2>Delete</h2>
	<form method="post">
		<label for="id">ID:</label>
		<input type="number" name="id" required>

		<input type="submit" name="delete" value="Delete">
	</form>

</body>
</html>
