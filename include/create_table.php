<?php

// check if the table exists
$result = mysqli_query($conn, "SHOW TABLES LIKE 'users'");
$tableExists = mysqli_num_rows($result) > 0;

if (!$tableExists) {
	$sql = "CREATE TABLE users (
			id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			name VARCHAR(50) NOT NULL,
			email VARCHAR(50) NOT NULL,
            password VARCHAR(30) NOT NULL
		)";

	if (mysqli_query($conn, $sql)) {
		
	} else {
		echo "Error creating table: " . mysqli_error($conn);
	}
}
?>