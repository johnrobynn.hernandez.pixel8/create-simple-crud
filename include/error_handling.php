<?php

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

if (mysqli_query($conn, $sql)) {
    echo "Query executed successfully";
} else {
    echo "Error executing query: " . mysqli_error($conn);
}
?>